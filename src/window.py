# window.py
#
# Copyright 2019 Jesus Bermudez Velazquez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import gi
from gi.repository import Gtk, GtkSource

from .utils import Request, Verb, ResterUrlNotValidException, ResterUrlNotFoundException

@Gtk.Template(resource_path='/org/example/App/window.ui')
class ResterWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'ResterWindow'

    new_url = Gtk.Template.Child()
    http_methods = Gtk.Template.Child()
    _send_request = Gtk.Template.Child()
    notebook = Gtk.Template.Child()
    grid = Gtk.Template.Child()
    response_grid = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._setup_view()

    def _setup_view(self):
        self._set_http_methods()
        # TODO: not set sensitive unless _send_request is not empty
        self._send_request.set_sensitive(True)
        self.new_url.set_placeholder_text('Enter request URL')
        scroll_body = Gtk.ScrolledWindow()
        self._setup_response_view()
        label = Gtk.Label()
        label.set_padding(5, 5)
        self.popover_error = Gtk.Popover.new(self.new_url)
        self.popover_error.set_size_request(100, 50)
        self.popover_error.set_position(Gtk.PositionType.BOTTOM)
        self.popover_error.add(label)
        self._send_request.connect('clicked', self.send_request, self.popover_error)

    def _set_http_methods(self):
        http_verbs = Verb().get_verbs()
        for i, http_verb in enumerate(http_verbs):
            self.http_methods.append(str(i), http_verb)

        self.http_methods.set_active(0)
        self.http_methods.set_sensitive(True)

    def _setup_response_view(self):
        # setup body and headers interface
        self.setup_response_body_headers_ui()
        # status and time labels
        self.setup_response_metadata()

    def setup_response_body_headers_ui(self):
        main_response_info_keys = ['Body', 'Headers']
        self.response_scrolledwindows = {
            'Body': Gtk.ScrolledWindow(),
            'Headers': Gtk.ScrolledWindow()
        }
        lang_mgr = GtkSource.LanguageManager()
        html_lang = lang_mgr.get_language('html')
        html_buffer = GtkSource.Buffer()
        html_buffer.set_language(html_lang)
        html_buffer.set_highlight_syntax(True)
        self.response_scrolledwindows.get('Body').add(
            GtkSource.View.new_with_buffer(html_buffer)
        )
        headers_lang = lang_mgr.get_language('json')
        headers_buffer = GtkSource.Buffer()
        headers_buffer.set_language(headers_lang)
        headers_buffer.set_highlight_syntax(True)
        self.response_scrolledwindows.get('Headers').add(
            GtkSource.View.new_with_buffer(headers_buffer)
        )
        for i, main_key in enumerate(main_response_info_keys):
            self.notebook.append_page(
                self.response_scrolledwindows.get(main_key),  # prepare scroll for response text
                Gtk.Label(main_key)
        )

    def setup_response_metadata(self):
        self.header_info_grid = Gtk.Grid()
        self.header_info_grid.set_column_spacing(20)
        labels = ['Status', 'Time', 'Size']

        for i, label in enumerate(labels):
            self.header_info_grid.attach(
                Gtk.Label(label), i, 0, 1, 1
            )

        self.header_info_grid.get_child_at(1,0).set_vexpand(True)
        self.header_info_grid.show_all()
        self.notebook.set_action_widget(self.header_info_grid, 1)
        self.notebook.show_all()
        self.notebook.set_hexpand(True)
        self.notebook.set_vexpand(True)

    def send_request(self, button, cosas):
        """
        Build the request and send it.
        """
        try:
            Request(
                self.new_url.get_text(),
                self.http_methods.get_active_text()
            ).send_request(
                self.popover_error,
                self.header_info_grid,
                self.response_scrolledwindows  # TODO: add callback to display here
            )
        except Exception as issue:
            self.popover_error.get_child().set_text('Rester can not send request: {0}'.format(issue))
            self.popover_error.show_all()
