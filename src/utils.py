# utils.py
#
# Copyright 2019 Jesus Bermudez Velazquez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import gi
gi.require_version('Soup', '2.4')
from gi.repository import Soup
import time
import json
import re
from .validators import UrlValidator
from .exceptions import ResterUrlNotFoundException, ResterUrlNotValidException

class Verb(object):
    def __init__(self):
        self.verbs = ['GET', 'POST', 'PUT', 'DELETE']

    def __repr__(self):
        return '<Verb>'

    def get_verbs(self):
        return self.verbs


class Request(object):

    def __init__(self, url=None, verb=None):
        # TODO: refine in a function for localhost
        # TODO: test localhost
        if not url.startswith('http'):
            url = 'https://{}'.format(url)

        try:
            if self._is_valid(url):
                self.url = Soup.URI.new(url)
                self.verb = verb
                # TODO: add auth, key-val params

        except ResterUrlNotFoundException as issue:
            raise ResterUrlNotFoundException(issue)
        except Exception as issue:
            raise ResterUrlNotValidException('Invalid url.')

    def __repr__(self):
        return '<Request>'

    def _is_valid(self, url):
        try:
            if not url:
                raise ResterUrlNotFoundException('The url is empty')

            url_validator = UrlValidator()
            is_valid_url = url_validator.url(url)
            if not is_valid_url:
                raise ResterUrlNotValidException('Invalid url')

            return is_valid_url
        except Exception as issue:
            raise Exception(issue)

    def send_request(self, error, header_info, body_headers_scrolledwindows):
        raw_response = Soup.Message.new_from_uri(
            self.verb,
            self.url
        )
        response = Response()
        response.set_start_response_time(time.process_time())
        pepe = response.session.queue_message(
            raw_response,
            response.handle_response,
            body_headers_scrolledwindows,
            True
        )


class Response(object):
    def __init__(self):
        self.response = {
            'body': '',
            'headers': '',
            'status_code': '',
            'method': '',
            'time': 0,
            'size': 0
        }
        self.session = Soup.SessionAsync()

    def __repr__(self):
        return '<Response>'

    def get_info(self, key):
        return self.response.get(key)

    def set_start_response_time(self, time):
        self.start = time

    def handle_response(self, session, msg, body_headers_scrolledwindows, data):
        self.response['time'] = time.process_time() - self.start
        self.response['body'] = msg.response_body
        self.response['status_code'] = msg.status_code
        self.response['method'] = msg.method
        body_headers_scrolledwindows.get('Body').get_child().set_show_line_numbers(True)
        body_headers_scrolledwindows.get('Body').get_child().get_buffer().set_text(self.response.get('body').data)

        headers_response = {}
        msg.response_headers.foreach(self._set_headers, headers_response)
        json_headers = json.dumps(
            headers_response,
            indent=4,
            sort_keys=True,
            ensure_ascii=False
        )
        self.response['headers'] = json_headers

        body_headers_scrolledwindows.get('Headers').get_child().set_show_line_numbers(True)
        body_headers_scrolledwindows.get('Headers').get_child().get_buffer().set_text(self.response.get('headers'))

    def _set_headers(name, value, data, headers_response):
        headers_response[value] = data

